FROM keymetrics/pm2:latest-alpine

RUN apk update && apk upgrade && \
  apk add --no-cache \
    bash \
    git \
    curl \
    openssh

ARG APPLICATION_NAME
ARG SECRET 
ARG PORT 
ARG API_KEY 
ARG IMAGE_URL
ARG NODE_ENV

ENV APPLICATION_NAME ${APPLICATION_NAME}
ENV SECRET ${SECRET}
ENV PORT ${PORT}
ENV API_KEY ${API_KEY}
ENV IMAGE_URL ${IMAGE_URL}
ENV NODE_ENV ${NODE_ENV}


RUN mkdir -p /usr/src/softdev
WORKDIR /usr/src/softdev
RUN echo "APPLICATION_NAME=${APPLICATION_NAME}" >> /usr/src/softdev/.env
RUN echo "SECRET=${SECRET}" >> /usr/src/softdev/.env
RUN echo "API_KEY=${API_KEY}" >> /usr/src/softdev/.env
RUN echo "PORT=${PORT}" >> /usr/src/softdev/.env
RUN echo "IMAGE_URL=${IMAGE_URL}" >> /usr/src/softdev/.env
RUN echo "NODE_ENV=${NODE_ENV}" >> /usr/src/softdev/.env

COPY package*.json ./
RUN npm cache clean --force
RUN npm install
COPY . .

EXPOSE 7000

CMD [ "pm2-runtime", "start", "pm2.json", "--env", "development"]
