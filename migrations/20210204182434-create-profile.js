'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('profiles', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      username: {
        type: Sequelize.STRING
      },
      nik: {
        allowNull: false,
        unique: true,
        type: Sequelize.BIGINT,
        references: { model: 'users', key: 'nik' }
      },
      phone: {
        type: Sequelize.STRING
      },
      homebase: {
        type: Sequelize.STRING
      },
      dob: {
        type: Sequelize.DATE
      },
      ktp_id: {
        type: Sequelize.BIGINT
      },
      address: {
        type: Sequelize.TEXT
      },
      jabatan: {
        type: Sequelize.STRING
      },
      divisi: {
        type: Sequelize.STRING
      },
      gender: {
        type: Sequelize.STRING
      },
      long: {
        type: Sequelize.DOUBLE
      },
      lat: {
        type: Sequelize.DOUBLE
      },
      avatar: {
        type: Sequelize.STRING
      },
      insurance_num: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        onUpdate : Sequelize.literal('CURRENT_TIMESTAMP'),
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('profiles');
  }
};