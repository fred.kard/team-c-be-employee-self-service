'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query("ALTER TABLE leaves ADD CONSTRAINT fk_leaves FOREIGN KEY (nik, type_approval) REFERENCES user_leaves (nik, type) ON DELETE cascade");
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("ALTER TABLE leaves DROP CONSTRAINT fk_leaves");
  }
};