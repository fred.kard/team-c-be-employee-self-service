'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('attendances', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nik: {
        type: Sequelize.BIGINT,
        references: { model: 'profiles', key: 'nik' }
      },
      date: {
        type: Sequelize.DATE
      },
      is_clockin: {
        type: Sequelize.BOOLEAN
      },
      clockin: {
        type: Sequelize.DATE
      },
      is_officeclockin: {
        type: Sequelize.BOOLEAN
      },
      is_clockout: {
        type: Sequelize.BOOLEAN
      },
      clockout: {
        type: Sequelize.DATE
      },
      is_officeclockout: {
        type: Sequelize.BOOLEAN
      },
      duration: {
        type: Sequelize.FLOAT
      },
      is_clockinapproved: {
        type: Sequelize.BOOLEAN
      },
      is_clockoutapproved: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        onUpdate : Sequelize.literal('CURRENT_TIMESTAMP'),
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('attendances');
  }
};