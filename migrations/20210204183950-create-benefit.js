'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('benefits', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nik: {
        type: Sequelize.BIGINT,
        references: { model: 'profiles', key: 'nik' }
      },
      type: {
        type: Sequelize.INTEGER
      },
      type_name: {
        type: Sequelize.STRING
      },
      type_limit: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        onUpdate: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
    });
    queryInterface.addConstraint('benefits', {
      type: 'unique',
      fields: ['nik', 'type'],
      name: 'nik_type',
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('benefits');
  }
};