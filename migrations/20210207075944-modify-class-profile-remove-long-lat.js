'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('profiles', 'long'),
      queryInterface.removeColumn('profiles', 'lat')
    ])
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'profles',
        'long',
        {
          type: Sequelize.DOUBLE
        }
      ),
      queryInterface.addColumn(
        'profiles',
        'lat',
        {
          type: Sequelize.DOUBLE,
        }
      ),
    ])
  }
};
