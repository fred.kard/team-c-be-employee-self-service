'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
          'profiles',
          'name',
          {
            type: Sequelize.STRING
          }
      ),
      queryInterface.addColumn(
          'profiles',
          'email',
          {
            type: Sequelize.STRING,
          }
      ),
    ])
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('profiles', 'name'),
      queryInterface.removeColumn('profiles', 'email')
    ])
  }
};
