'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
          'profiles',
          'no_kk',
          {
            type: Sequelize.BIGINT
          }
      )
    ])
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('profiles', 'no_kk')
    ])
  }
};
