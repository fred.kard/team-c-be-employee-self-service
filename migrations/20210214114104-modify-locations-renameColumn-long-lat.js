'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.renameColumn('locations','long', 'latitude', {type: Sequelize.DOUBLE}),
      queryInterface.renameColumn('locations','lat', 'longitude', {type: Sequelize.DOUBLE})
    ])
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.renameColumn('locations','latitude', 'long', {type: Sequelize.DOUBLE}),
      queryInterface.renameColumn('locations','longitude', 'lat', {type: Sequelize.DOUBLE})
    ])
  }
};
