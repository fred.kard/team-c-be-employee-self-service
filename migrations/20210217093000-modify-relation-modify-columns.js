'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.changeColumn('relations', 'relation_status', {
        type: Sequelize.ENUM('spouse','ayah','ibu','anak','anak ke 1','anak ke 2','anak ke 3','anak ke 4','anak ke 5'),
        allowNull: true,
      })
    ])
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.changeColumn('relations', 'relation_status', {
        type: Sequelize.STRING,
        allowNull: true,
      })
    ])
  }
};