'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('bookings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nik: {
        type: Sequelize.BIGINT,
        references: { model: 'profiles', key: 'nik' }
      },
      room_id: {
        type: Sequelize.INTEGER,
        references: { model: 'rooms', key: 'id' }
      },
      time: {
        type: Sequelize.DATE
      },
      description: {
        type: Sequelize.TEXT
      },
      is_projector: {
        type: Sequelize.BOOLEAN
      },
      is_hdmi: {
        type: Sequelize.BOOLEAN
      },
      is_board: {
        type: Sequelize.BOOLEAN
      },
      is_consumption: {
        type: Sequelize.BOOLEAN
      },
      consumption: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        onUpdate : Sequelize.literal('CURRENT_TIMESTAMP'),
      }
    });

   

  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('bookings');
  }
};