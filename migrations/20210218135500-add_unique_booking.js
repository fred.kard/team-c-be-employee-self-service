'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.addConstraint('bookings',  {
      type: 'unique',
      fields: ['room_id', 'time'],
      name: 'room_time',
    });

    queryInterface.addConstraint('bookings', {
      type: 'unique',
      fields: ['nik', 'time'],
      name: 'nik_time',
    });
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.removeConstraint('bookings', 'room_time');
    queryInterface.removeConstraint('bookings', 'nik_time');
  }
};
