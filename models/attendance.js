'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class attendance extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.profile, {foreignKey:"nik", targetKey: "nik"});
    }
  };
  attendance.init({
    nik: DataTypes.BIGINT,
    date: DataTypes.DATEONLY,
    is_clockin: DataTypes.BOOLEAN,
    clockin: DataTypes.DATE,
    is_officeclockin: DataTypes.BOOLEAN,
    is_clockout: DataTypes.BOOLEAN,
    clockout: DataTypes.DATE,
    is_officeclockout: DataTypes.BOOLEAN,
    duration: DataTypes.FLOAT,
    is_clockinapproved: DataTypes.BOOLEAN,
    is_clockoutapproved: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'attendance',
  });
  return attendance;
};