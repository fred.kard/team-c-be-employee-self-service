'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class benefit extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      this.hasMany(models.insurance, { foreignKey: "type",sourceKey: "type" });
      this.hasMany(models.insurance, { foreignKey: "nik",sourceKey: "nik" });
      this.hasMany(models.medical, { foreignKey: "type",sourceKey: "type" });
      this.hasMany(models.medical, { foreignKey: "nik",sourceKey: "nik" });

    }
  };
  benefit.init({
    nik: DataTypes.BIGINT,
    type: DataTypes.INTEGER,
    type_name: DataTypes.STRING,
    type_limit: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'benefit',
  });
  return benefit;
};