'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class booking extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.profile, {foreignKey:"nik", targetKey: "nik"});
      this.belongsTo(models.room, {foreignKey:"room_id", targetKey: "id"});
    }
  };
  booking.init({
    nik: DataTypes.BIGINT,
    room_id: DataTypes.INTEGER,
    time: DataTypes.DATE,
    description: DataTypes.TEXT,
    is_projector: DataTypes.BOOLEAN,
    is_hdmi: DataTypes.BOOLEAN,
    is_board: DataTypes.BOOLEAN,
    is_consumption: DataTypes.BOOLEAN,
    consumption: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'booking',
  });
  return booking;
};