'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class insurance extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.benefit, { foreignKey: "type",targetKey: "type"});
    }
  };
  insurance.init({
    nik: DataTypes.BIGINT,
    year: DataTypes.INTEGER,
    type: DataTypes.INTEGER,
    type_usage: DataTypes.INTEGER,
    type_balance: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'insurance',
  });
  return insurance;
};