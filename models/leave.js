'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class leave extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.user_leave, {foreignKey: "nik"});
    }
  };
  leave.init({
    nik: DataTypes.BIGINT,
    start_date: DataTypes.DATE,
    end_date: DataTypes.DATE,
    reason: DataTypes.STRING,
    duration: DataTypes.INTEGER,
    type_approval: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'leave',
  });
  return leave;
};