'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class location extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.profile, {foreignKey:"nik", as:"profile"})
    }
  };
  location.init({
    nik: DataTypes.BIGINT,
    long: DataTypes.DOUBLE,
    lat: DataTypes.DOUBLE
  }, {
    sequelize,
    modelName: 'location',
  });
  return location;
};