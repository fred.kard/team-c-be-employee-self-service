'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class log extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  log.init({
    user_id: DataTypes.INTEGER,
    api: DataTypes.STRING,
    header: DataTypes.TEXT,
    body: DataTypes.TEXT,
    result_status: DataTypes.INTEGER,
    result_message: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'log',
  });
  return log;
};