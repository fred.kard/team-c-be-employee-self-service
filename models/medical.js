'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class medical extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.benefit, { foreignKey: "type",targetKey: "type"});
    }
  };
  medical.init({
    nik: DataTypes.BIGINT,
    date: DataTypes.DATE,
    year: DataTypes.INTEGER,
    type: DataTypes.INTEGER,
    usage: DataTypes.BIGINT
  }, {
    sequelize,
    modelName: 'medical',
  });
  return medical;
};