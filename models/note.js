'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class note extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.profile, {foreignKey: "nik"});
    }
  };
  note.init({
    nik: DataTypes.BIGINT,
    date: DataTypes.DATEONLY,
    description: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'note',
  });
  return note;
};