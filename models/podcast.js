'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class podcast extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  podcast.init({
    url: DataTypes.STRING,
    title: DataTypes.STRING,
    artist: DataTypes.STRING,
    artwork: DataTypes.STRING,
    duration: DataTypes.INTEGER,
    description: DataTypes.TEXT('long'),
    published_date: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'podcast',
  });
  return podcast;
};