'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class profile extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.user, {foreignKey: "nik"});
      this.hasMany(models.location, {foreignKey:"nik", sourceKey:"nik", as:"location"});
      this.hasMany(models.attendance, {foreignKey:"nik", sourceKey:"nik", as:"attendance"});
      this.hasMany(models.relation, {foreignKey:"nik", sourceKey:"nik", as:"relation"});
      this.hasMany(models.booking, {foreignKey:"nik", sourceKey:"nik", as:"booking"});
    }
  };
  profile.init({
    username: DataTypes.STRING,
    nik: DataTypes.BIGINT,
    phone: DataTypes.STRING,
    homebase: DataTypes.STRING,
    dob: DataTypes.DATE,
    ktp_id: DataTypes.BIGINT,
    address: DataTypes.TEXT,
    jabatan: DataTypes.STRING,
    divisi: DataTypes.STRING,
    gender: DataTypes.STRING,
    avatar: DataTypes.STRING,
    insurance_num: DataTypes.STRING,
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    no_kk: DataTypes.BIGINT,
  }, {
    sequelize,
    modelName: 'profile',
  });
  return profile;
};