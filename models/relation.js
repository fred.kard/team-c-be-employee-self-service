'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class relation extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.profile, {foreignKey:"nik", as:"profile"});
    }
  };
  relation.init({
    nik: DataTypes.BIGINT,
    name: DataTypes.STRING,
    relation_status: DataTypes.STRING,
    dob: DataTypes.DATE,
    address: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'relation',
  });
  return relation;
};