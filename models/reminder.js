'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class reminder extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.profile, {foreignKey: "nik"});
    }
  };
  reminder.init({
    nik: DataTypes.BIGINT,
    time_start: DataTypes.DATE,
    time_stop: DataTypes.DATE,
    description: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'reminder',
  });
  return reminder;
};