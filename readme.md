<div align="center">

<center>
<h1>Team C SoftDev Academy - Employee Self Service</h1>
<img src="data/moana2.1-logo.png" class="center">
</center>
</div>


## Back End Team
- Arvin Yardhika - Tech Lead
- Bagus Setyawan - Team Member
- Diana Yurika Littik - Team Member
- Emir Septian Sori Dongoran - Team Member
- Fredy Kardian - Team Member
- Naufal Hariz - Team Member

# Overview 
Moana 2.1 merupakan aplikasi self-service bagi karyawan untuk melakukan pencatatan kehadiran, izin untuk meninggalkan pekerjaan, klaim benefit kesehatan, dan fitur-fitur penunjang pekerjaan yang lainnya

# Brief Explanation
Project ini memiliki total 35 API yang digunakan untuk 8 fitur besar yang tersedia

# Postman Collection Endpoint API
Semua API yang ada dapat diakses melalui link berikut https://documenter.getpostman.com/view/602784/TW76E5BA

## Main Feature
- Absence : Untuk mencatatkan kehadiran karyawan
- Profile : Untuk melihat dan merubah profile karyawan
- Leave   : Untuk mengajukan cuti dan izin
- Medical Benefit : Untuk melihat info medical benefit dan mengajukan klaim
- Calendar : Untuk melihat notes atau reminder

## Extra Feature
- News    : Untuk melihat Corporate News
- Podcast : Untuk mendengarkan podcast 
- Booking Room : Untuk melakukan booking ruangan meeting 

# Authentication
## Overview
Fitur ini memiliki 3 fungsi utama, yaitu:
- Login, untuk user menggunakan fungsi Login
- Check Password, untuk mengecek password user apakah telah lebih dari 30 hari, apabila ya akan diminta untuk mengganti password
- Change Password, untuk mengganti password
- Forget Password, apabila user lupa password, password yang baru akan dikirim ke email user dan diminta untuk mengganti passwordnya

# Absence
## Overview
Fitur ini memiliki fungsi sebagai berikut:
- Melakukan clock-in dan clock-out
- Mengecek history kehadiran
- Menggunakan polygon untuk menentukan apakah user berada di dalam/luar kantornya

# Leave
## Overview
Fitur ini memiliki fungsi sebagai berikut:
- Mengecek saldo ijin/cuti yang dimiliki oleh user
- Melakukan permohonan ijin/cuti bagi user
- Melakukan pengecekan ijin/cuti dari user

# Medical Benefit
## Overview
Fitur ini memiliki fungsi sebagai berikut:
- Mengecek semua benefit kesehatan yang dimiliki user
- Melakukan klaim untuk benefit kesehatan yang dimiliki user
- Mengecek semua history pemakaian klaim yang dimiliki user

# Calendar
## Overview
Fitur ini memiliki fungsi sebagai berikut:
- Melakukan input untuk reminder (jam dan tanggal)
- Menghapus reminder
- Melakukan edit pada reminder terinput
- Melakukan input untuk notes (tanggal)
- Menghapus notes
- Melakukan edit pada notes terinput 

# News
## Overview
Fitur ini memiliki fungsi sebagai berikut:
- Menyediakan news tentang berita korporasi kepada user

# Podcast
## Overview
Fitur ini memiliki fungsi sebagai berikut:
- Memutar Podcast yang dapat dinikmati oleh karyawan

# Booking Room
## Overview
Fitur ini memiliki fungsi sebagai berikut:
- Mengecek ketersediaan seluruh Room
- Mengecek jadwal booking yang sudah dibuat untuk seluruh Room
- Mengecek jadwal booking yang telah dibuat user
- Melakukan booking untuk room pada jadwal dan ruangan yang diinginkan
- Menghapus booking yang sudah dibuat

# Restriction
Pada Aplikasi ini semua HTTP Request harus menggunakan API_KEY(validasi request) dan menggunakan JWT Token(validasi user)

Apabila dalam penggunaan aplikasi terdapat masalah, dapat menghubungi tim yang terlampir di atas, terima kasih :)
