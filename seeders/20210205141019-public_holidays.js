'use strict';
const fs = require('fs');
const path = require('path');
module.exports = {
  up: async (queryInterface, Sequelize) => {
    const data = fs.readFileSync(path.join(__dirname, 'data', 'public_holidays.json'), 'utf8');
    const searchForm = JSON.parse(data);
 
    return queryInterface.bulkInsert('public_holidays', searchForm);
  },

  down: queryInterface => queryInterface.bulkDelete('public_holidays', null, {}),
};
