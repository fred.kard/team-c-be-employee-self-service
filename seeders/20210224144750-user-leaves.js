'use strict';

const fs = require('fs');
const path = require('path');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const data = fs.readFileSync(path.join(__dirname, 'data', 'user_leaves.json'), 'utf8');
    const searchForm = JSON.parse(data);

    return queryInterface.bulkInsert('user_leaves', searchForm);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user_leaves', null, {});
  }
};
