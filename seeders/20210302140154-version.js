'use strict';

const fs = require('fs');
const path = require('path');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const data = fs.readFileSync(path.join(__dirname, 'data', 'versions.json'), 'utf8');
    const searchForm = JSON.parse(data);

    return queryInterface.bulkInsert('versions', searchForm);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('versions', null, {});
  }
};
