const Sequelize = require("sequelize");
const UserModel = require("./models/user.js");
const LogModel = require("./models/log.js");
const AttendanceModel = require("./models/attendance.js");
const IndexModel = require("./models/insurance.js");
const InsuranceModel = require("./models/insurance.js");
const LeaveModel = require("./models/leave.js");
const MedicalModel = require("./models/medical.js");
const NoteModel = require("./models/note.js");
const ProfileModel = require("./models/profile.js");
const PublicHolidayModel = require("./models/public_holiday.js");
const RelationModel = require("./models/relation.js");
const ReminderModel = require("./models/reminder.js");
const UserLeaveModel = require("./models/user_leave.js");
const locationModel = require("./models/location");
const newsModel = require("./models/news");
const podcastModel = require("./models/podcast");
const roomModel = require("./models/room");
const bookingModel = require("./models/booking");

const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/config/config.json')[env];
const db = {};
if (config.use_env_variable) {
    sequelize = new Sequelize(process.env[config.use_env_variable], config);
  } else {
    sequelize = new Sequelize(config.database, config.username, config.password, config);
  }

const user = UserModel(sequelize,Sequelize);
sequelize.sync().then(()=>{
    console.log(`User table have been created`)
});
const log = LogModel(sequelize,Sequelize);
sequelize.sync().then(()=>{
    console.log(`Log Table have been created`)
});

const attendance = AttendanceModel(sequelize,Sequelize);
sequelize.sync().then(()=>{
    console.log(`Attendance table have been created`)
});
const index = IndexModel(sequelize,Sequelize);
sequelize.sync().then(()=>{
    console.log(`Review table have been created`)
});

const insurance = InsuranceModel(sequelize,Sequelize);
sequelize.sync().then(()=>{
    console.log(`Insurance table have been created`)
});
const leave = LeaveModel(sequelize,Sequelize);
sequelize.sync().then(()=>{
    console.log(`Leave table have been created`)
});
const medical = MedicalModel(sequelize,Sequelize);
sequelize.sync().then(()=>{
    console.log(`Medical table have been created`)
});
const note = NoteModel(sequelize,Sequelize);
sequelize.sync().then(()=>{
    console.log(`Note table have been created`)
});
const profile = ProfileModel(sequelize,Sequelize);
sequelize.sync().then(()=>{
    console.log(`PRofile table have been created`)
});
const public_holiday = PublicHolidayModel(sequelize,Sequelize);
sequelize.sync().then(()=>{
    console.log(`Public Holiday table have been created`)
});
const relation = RelationModel(sequelize,Sequelize);
sequelize.sync().then(()=>{
    console.log(`Relation table have been created`)
});
const reminder = ReminderModel(sequelize,Sequelize);
sequelize.sync().then(()=>{
    console.log(`Reminder table have been created`)
});
const user_leave = UserLeaveModel(sequelize,Sequelize);
sequelize.sync().then(()=>{
    console.log(`User Leave table have been created`)
});
const location = locationModel(sequelize,Sequelize);
sequelize.sync().then(()=>{
    console.log(`location table have been created`)
});

const news = newsModel(sequelize,Sequelize);
sequelize.sync().then(()=>{
    console.log(`News table have been created`)
});

const podcast = podcastModel(sequelize,Sequelize);
sequelize.sync().then(()=>{
    console.log(`podcast table have been created`)
});

const room = roomModel(sequelize,Sequelize);
sequelize.sync().then(()=>{
    console.log(`podcast table have been created`)
});

const booking = bookingModel(sequelize,Sequelize);
sequelize.sync().then(()=>{
    console.log(`podcast table have been created`)
});


exports.user = user;
exports.log = log;
exports.attendance = attendance;
exports.index = index;
exports.insurance = insurance;
exports.leave = leave;
exports.medical = medical;
exports.note = note;
exports.profile = profile;
exports.public_holiday = public_holiday;
exports.relation = relation;
exports.reminder = reminder;
exports.user_leave = user_leave;
exports.location = location;
exports.news = news;
exports.podcast = podcast;
exports.room = room;
exports.booking = booking;