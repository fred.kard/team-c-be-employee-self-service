class Utility {
    static MESSAGE_DATA_FOUND = "Data found";
    static MESSAGE_DATA_NOT_FOUND = "Data not found";
    static MESSAGE_SUCCESS_INSERT = "Data has been inserted";
    static MESSAGE_FAILED_INSERT = "Insert failed";
    static MESSAGE_FAILED_BALANCE = "Update failed";
    static MESSAGE_FAILED_SELECT = "Get data failed";

    static STATE_SUCCESS = "Success";
    static STATE_FAILED = "Failed";

    static CODE_SUCCESS = "200"
    static CODE_FAILED = "500"
}

module.exports = Utility