const model = require("../../../sequelize.js");
const user = model.user;
const attendance = model.attendance;
const log = model.log;
const index = model.index;
const insurance = model.insurance;
const leave = model.leave;
const medical = model.medical;
const note = model.note;
const profile = model.profile;
const public_holiday = model.public_holiday;
const relation = model.relation;
const reminder = model.reminder;
const user_leave = model.user_leave;
const location = model.location;
const news = model.news;
const podcast = model.podcast;
const room = model.room;
const booking = model.booking;

module.exports = {
    DatabaseMiddleware: (req, res, next) => {
        req.user = user;
        req.log = log;
        req.attendance = attendance;
        req.index = index;
        req.insurance = insurance;
        req.leave = leave;
        req.medical = medical;
        req.note = note;
        req.profile = profile;
        req.public_holiday = public_holiday;
        req.relation = relation;
        req.reminder = reminder;
        req.user_leave = user_leave;
        req.location = location;
        req.news = news;
        req.podcast = podcast;
        req.room = room;
        req.booking = booking;
        next()
    }
};
