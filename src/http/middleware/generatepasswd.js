const crypto = require('crypto')

module.exports = {
    generatepasswdMiddleware: async (req, res, next) => {
        const generatePassword = (
            length = 20,
            wishlist = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz~!@-#$'

        ) =>
        Array.from(crypto.randomFillSync(new Uint32Array(length)))
        .map((x) => wishlist[x % wishlist.length])
        .join('')
    req.generatePassword = generatePassword()
    next()
    return
}
}
