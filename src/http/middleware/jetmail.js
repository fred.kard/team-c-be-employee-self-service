const mailjet = require('node-mailjet')

    .connect('3a913705fc4b964e6ca6925ee5dcda16', '23f885e269e82f1b3b616f90032a7b05')

module.exports = {
    jetmailMiddleware: async (req, res, next) =>{
        const { email } = req.body
        const mjml = req.mjml
        console.log(mjml)
        const request = mailjet
        .post("send", {
            'version': 'v3.1'
        })
        .request({
            "Messages": [{
                "From": {
                    "Email": "fred.kard@gmail.com",
                    "Name": "Employee Self Service" 
                },
                "To": [{
                    "Email": `${email}`,
                    "Name": "Nama"
                }],
                "Subject": "Password Reset Request.",
                "TextPart": "This is your new password, you will be asked to change upon login",
                "HTMLPart": mjml['html'],
                "CustomID": "Sending New Password"
            }]
        })
    request
        .then((result) => {
            console.log(result.body)
        })
        .catch((err) => {
            console.log(err.statusCode)
        })
        next()
        return
    }
}
