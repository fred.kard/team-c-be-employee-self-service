const mjml2html = require('mjml')

/*
  Compile an mjml string
*/
module.exports = {
    mjmlMiddleware:async(req, res, next) =>{
        const newPassword = req.generatePassword
        const htmlOutput = mjml2html(`
        <mjml>
        <mj-body>
          <!-- Company Header -->
          <mj-section background-color="#f0f0f0">
            <mj-column>
              <mj-text font-style="italic" font-size="20px" color="#626262">
                MOANA 2.1
              </mj-text>
            </mj-column>
          </mj-section>
          <mj-section background-url="http://119.8.188.194/tlt.png" background-size="cover" background-repeat="no-repeat">
      
            <mj-column width="600px">
      
              <mj-text align="center" color="#fff" font-size="40px" font-family="Lato">Office In Your Hand</mj-text>
      
              
            </mj-column>
      
          </mj-section>
      
      
      
          <!-- Intro text -->
          <mj-section background-color="#fafafa">
            <mj-column width="400px">
      
              <mj-text font-style="italic" font-size="20px" font-family="Lato" color="#626262">Hi, Semangat Pagi</mj-text>
      
            </mj-column>
            <mj-column width="400px">
            <mj-text font-size="14px" font-family="Lato" color="#626262">
                This is your new password, you will be asked to change password upon login
              
              </mj-text>
            </mj-column>
            <mj-column width="400px">
            <mj-text align=center font-size="14px" font-family="Lato" color="#626262">
            
              ${newPassword}

            </mj-text>
          </mj-column>
          </mj-section>
          <!-- Icons -->
          <mj-section background-color="#fbfbfb">
            
          </mj-section>
          <mj-section background-color="#fbfbfb">
            <mj-column width="100%">
              <mj-text align="center" color="#626262" font-size="20px" font-family="Lato">Moana 2.1 Extra Feature</mj-text>
            </mj-column>
            <mj-column>
              <mj-image width="200px" src="http://119.8.188.194/NEWS.png" />
            </mj-column>
            <mj-column>
              <mj-image width="200px" src="http://119.8.188.194/BOOKINGROOM.png" />
            </mj-column>
            <mj-column>
              <mj-image width="200px" src="http://119.8.188.194/PODCAST.png" />
            </mj-column>
          </mj-section>
          <mj-section background-color="#e7e7e7">
            <mj-column>
              <mj-social>
                <mj-social-element name="instagram" />
                <mj-social-element name="twitter" />
                <mj-social-element name="youtube" />
              </mj-social>
            </mj-column>
          </mj-section>
        </mj-body>
      </mjml>   
`)
        req.mjml = htmlOutput
        next()
        return
    } 
}