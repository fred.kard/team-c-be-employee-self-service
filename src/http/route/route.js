const express = require ('express');
const VersionController = require('../../logic/version/version.controller');
const authController = require('../../logic/auth/auth.controller');
const calendarController = require('../../logic/calendar/calendar.controller');
const userLeaveController = require('../../logic/user_leave/user_leave.controller');
const leaveController = require('../../logic/leave/leave.controller');
const notesController = require('../../logic/notes/notes.controller');
const imageController = require('../../logic/image/image.controller');
const newsController = require('../../logic/news/news.controller');
const podcastController = require('../../logic/podcast/podcast.controller');
const ProfileController = require('../../logic/profile/profile.controller');
const AttendanceController = require('../../logic/attendance/attendance.controller');
const insuranceController = require('../../logic/insurance/insurance.controller')
const meetingController = require('../../logic/meeting/meeting.controller')
const {JWTMiddleware} = require ('../middleware/JWT.middleware');
const {apikeyMiddleware} = require ('../middleware/apikey.middleware');
const {DatabaseMiddleware } = require('../middleware/database.middleware');
const {passwordMiddleware } = require('../middleware/password.middleware');
const {emailMiddleware} = require('../middleware/email.middleware');
const {generatepasswdMiddleware} = require('../middleware/generatepasswd');
const {jetmailMiddleware} = require('../middleware/jetmail');
const {mjmlMiddleware} = require('../middleware/mjml.middleware');

const router = express.Router();

const ac = new authController();
const cc = new calendarController();
const ulc = new userLeaveController();
const lc = new leaveController();
const nc = new notesController();
const attCon = new AttendanceController();
const ic = new imageController();
const nec = new newsController();
const pc = new podcastController();
const insCo = new insuranceController();
const proc = new ProfileController();
const mc = new meetingController();
const verCon = new VersionController();

router.post('/version/check', DatabaseMiddleware, apikeyMiddleware, verCon.checkVersion);
router.post('/version/latest', DatabaseMiddleware, apikeyMiddleware, verCon.latestVersion);

router.post('/auth/login', DatabaseMiddleware, apikeyMiddleware, ac.auth);
router.post('/auth/edit', DatabaseMiddleware, JWTMiddleware, apikeyMiddleware, passwordMiddleware, ac.edit);
router.get('/auth/checkexpired', DatabaseMiddleware, JWTMiddleware, apikeyMiddleware, ac.checkexpired);
router.post('/auth/forget', DatabaseMiddleware, apikeyMiddleware, emailMiddleware,generatepasswdMiddleware,mjmlMiddleware,jetmailMiddleware, ac.forget);

router.get('/calendar/holiday', DatabaseMiddleware, JWTMiddleware, apikeyMiddleware, cc.holiday);
router.post('/calendar/reminder/insert', DatabaseMiddleware, JWTMiddleware,apikeyMiddleware, cc.insert);
router.get('/calendar/reminder', DatabaseMiddleware, JWTMiddleware,apikeyMiddleware, cc.show);
router.post('/calendar/reminder/delete', DatabaseMiddleware, JWTMiddleware,apikeyMiddleware, cc.delete);
router.post('/calendar/reminder/edit', DatabaseMiddleware, JWTMiddleware,apikeyMiddleware, cc.edit);
router.post('/calendar/notes/insert', DatabaseMiddleware, JWTMiddleware,apikeyMiddleware, nc.insert);
router.get('/calendar/notes', DatabaseMiddleware, JWTMiddleware,apikeyMiddleware, nc.show);
router.post('/calendar/notes/delete', DatabaseMiddleware, JWTMiddleware,apikeyMiddleware, nc.delete);
router.post('/calendar/notes/edit', DatabaseMiddleware, JWTMiddleware,apikeyMiddleware, nc.edit);
router.get('/show/:image_name', JWTMiddleware,apikeyMiddleware, ic.show);
router.post('/upload', DatabaseMiddleware, JWTMiddleware,apikeyMiddleware, ic.upload);
router.get('/getavatar', DatabaseMiddleware, JWTMiddleware,apikeyMiddleware, ic.get);
router.get('/news', DatabaseMiddleware, JWTMiddleware,apikeyMiddleware, nec.show);
router.get('/podcast', DatabaseMiddleware, JWTMiddleware,apikeyMiddleware, pc.show);
router.post('/meeting/book', DatabaseMiddleware, JWTMiddleware,apikeyMiddleware, mc.book);
router.post('/meeting/delete', DatabaseMiddleware, JWTMiddleware,apikeyMiddleware, mc.cancel);
router.get('/meeting/mybookings', DatabaseMiddleware, JWTMiddleware,apikeyMiddleware, mc.mybooking);
router.get('/meeting/room', DatabaseMiddleware, JWTMiddleware,apikeyMiddleware, mc.room);

router.get('/getprofile', DatabaseMiddleware, JWTMiddleware,apikeyMiddleware, proc.getprofile);
router.put('/profile/edit', DatabaseMiddleware, JWTMiddleware,apikeyMiddleware,proc.updateProfile);
router.post('/relation/add', DatabaseMiddleware, JWTMiddleware,apikeyMiddleware, proc.addRelation);
router.put('/relation/edit', DatabaseMiddleware, JWTMiddleware,apikeyMiddleware, proc.updateRelation);

router.get('/attendance/profile', DatabaseMiddleware, JWTMiddleware, apikeyMiddleware, attCon.profile);
router.post('/attendance/submit', DatabaseMiddleware, JWTMiddleware, apikeyMiddleware, attCon.submit);
router.get('/attendance/history', DatabaseMiddleware, JWTMiddleware, apikeyMiddleware, attCon.history);
// START - emir
router.get('/user_leave/getBalance', DatabaseMiddleware, JWTMiddleware, apikeyMiddleware, ulc.getBalance);
router.post('/leave/insertLeave',DatabaseMiddleware, JWTMiddleware, apikeyMiddleware, lc.insertLeave);
router.get('/leave/getHistory',DatabaseMiddleware, JWTMiddleware, apikeyMiddleware, lc.getHistory);
// END - emir
router.post('/insurance/insertclaim', DatabaseMiddleware, JWTMiddleware, apikeyMiddleware, insCo.insertclaim);
router.post('/insurance/allbenefit', DatabaseMiddleware, JWTMiddleware, apikeyMiddleware, insCo.getAllBenefit);
router.post('/insurance/allinfo', DatabaseMiddleware, JWTMiddleware, apikeyMiddleware, insCo.getAllInfo);

module.exports = router;
