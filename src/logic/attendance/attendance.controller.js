const responseHelper = require('../../helper/response.helper');
const strEntity = "Attendance";
const jwt = require('jsonwebtoken');
const models = require('../../../models');



class AttendanceController {
  async profile(req, res, next) {
    if (req.businessLogic != undefined) {
      next()
      return
    };

    const profile = models.profile;
    const attendance = models.attendance;
    const location = models.location;

    try {
      const attProfile = await profile.findAll({
        where : { nik: req.user_id },
        include:[
          {
            model:location,
            as: "location",
            attributes: ["latitude", "longitude"]
          },
          {
            model:attendance,
            as: "attendance",
            attributes: ["id", "date", "is_clockin", "clockin", "is_clockout", "clockout"],
            limit: 1,
            order: [["createdAt", "DESC"]]
          }
        ]
      });

      if (attProfile.length == 0) {
        req.businessLogic = await responseHelper({
          "code": 200,
          "message": "Attendance Profile not found",
          "entity": strEntity,
          "state": "RetreiveEmployeeProfileSucces",
          "data": []
        })
        next()
        return
      } ;

      req.businessLogic = await responseHelper({
        "code": 200,
        "message": "Attendance Profile found",
        "entity": strEntity,
        "state": "RetreiveEmployeeProfileSucces",
        "data": attProfile
      })
      next()
      return
      
    } catch(err) {
      console.log("error:", err);
      req.businessLogic = await responseHelper({
        "code": 500,
        "entity": strEntity,
        "state": "retrivedAttendanceProfileFailed",
      })
      next()
      return
    }
  }

  async history(req, res, next) {
    if (req.businessLogic != undefined) {
      next()
      return
    };
 
    const attendance = req.attendance;

    try {
      const attHistory = await attendance.findAll({
        where: { nik: req.user_id }
      })

      if (attHistory == 0) {
        req.businessLogic = await responseHelper({
          "code": 200,
          "message": "Attendance history not found",
          "entity": strEntity,
          "state": "RetreiveAttendanceHistorySucces",
          "data": []
        })
        next()
        return
      }

      req.businessLogic = await responseHelper({
        "code": 200,
        "message": "Attendance history found",
        "entity": strEntity,
        "state": "RetreiveAttendanceHistorySucces",
        "data": attHistory
      })
      next()
      return
      
    } catch(err) {
      req.businessLogic = await responseHelper({
        "code": 500,
        "entity": strEntity,
        "state": "retrivedAttendanceHistoryFailed",
      })
      next()
      return
    }
  }

  async submit(req, res, next) {
    if (req.businessLogic != undefined) {
      next()
      return
    };
    
    const { type, date, location } = req.body;
    const attendance = models.attendance;
    const newdate = new Date();
    const today = newdate.toISOString().slice(0,10);

    const checkDate = date.slice(0,10);

    
    if ( type == undefined || type == null ||date == undefined || date == null || 
      location == undefined || location == null )
      {
        req.businessLogic = await responseHelper({
          "code": 422,
          "message": "Attendance Data not complete",
          "entity": strEntity,
          "state": "SubmitAttendanceFailed"
        })
        next()
        return
      }

    if ( (type != "clock in" && type != "clock out") || (location != "in office" && location != "out office") ||
      (checkDate != today )) {
      req.businessLogic = await responseHelper({
        "code": 422,
        "message": "Attendance Data not valid",
        "entity": strEntity,
        "state": "SubmitAttendanceFailed"
      })
      next()
      return
    }
    var inOffice = false;

    if (location == "in office") {
      inOffice = true;
    };

    try {
      if (type == "clock in") {
        const checkAtt = await attendance.findOne({
          where: {
            nik: req.user_id, 
            date: today,
            is_clockin: true,
            is_clockout: false
          }
        })

        if (checkAtt == null) {
          const submitAtt = await attendance.create({
            nik: req.user_id,
            date: today,
            is_clockin: true,
            clockin: date,
            is_officeclockin: inOffice,
            is_clockout: false,
            clockout: null,
            is_officeclockout: null,
            duration: null,
            is_clockinapproved: null,
            is_clockoutapproved: null
          })
  
          req.businessLogic = await responseHelper({
            "code": 200,
            "message": "Attendance Submitted",
            "entity": strEntity,
            "state": "SubmitAttendanceSuccess",
            "data": submitAtt
          })
          next()
          return

        } else {
          req.businessLogic = await responseHelper({
            "code": 422,
            "message": "Clock in failed: clock in data existed",
            "entity": strEntity,
            "state": "SubmitAttendanceFailed",
            "data": checkAtt
          })
          next()
          return
        }
      } else {
        const checkAtt = await attendance.findOne({
          where: {
            nik: req.user_id, 
            date: today,
            is_clockin: true,
            is_clockout: false
          }
        })

        if (checkAtt != null) {
          const isClockIn = checkAtt.getDataValue('is_clockin');
          const attClockIn = checkAtt.getDataValue("clockin");
          const isOfficeClockIn = checkAtt.getDataValue("is_officeclockin")
          const isClockInApr = checkAtt.getDataValue("is_clockinapproved");
          const isClockOutApr = checkAtt.getDataValue("is_clockoutapproved");
          
          const startTime = new Date(date);
          const endTime = new Date (attClockIn);

          const workDur = (startTime - endTime)/1000; //in second
          
          if(workDur < 0) {
            req.businessLogic = await responseHelper({
              "code": 422,
              "message": "Clock out failed: clock out time must be after clock in time",
              "entity": strEntity,
              "state": "SubmitAttendanceFailed",
              "data": checkAtt
            })
            next()
            return
          }
          
          const updateAtt = await attendance.update(
            {
              nik: req.user_id,
              date: today,
              is_clockin: isClockIn,
              clockin: attClockIn,
              is_officeclockin: isOfficeClockIn,
              is_clockout: true, 
              clockout: date,
              is_officeclockout: inOffice,
              duration: workDur,
              is_clockinapproved: isClockInApr,
              is_clockoutapproved: isClockOutApr
            },
            {
              where: { 
                nik : req.user_id,
                date: today,
                is_clockin: true,
                is_clockout: false
              }
            }
          )
    
          req.businessLogic = await responseHelper({
            "code": 200,
            "message": "Attendance Submitted",
            "entity": strEntity,
            "state": "SubmitAttendanceSuccess",
            "data": {updateAtt}
          })
          next()
          return

        } else {
          req.businessLogic = await responseHelper({
            "code": 422,
            "message": "Clock out failed: clock in not found or already clock out",
            "entity": strEntity,
            "state": "SubmitAttendanceFailed",
            "data": checkAtt
          })
          next()
          return
        }
      }
    } catch(err) {
      req.businessLogic = await responseHelper({
        "code": 500,
        "entity": strEntity,
        "state": "SubmitAttendanceFailed",
      })
      next()
      return
    }
  }
}

module.exports = AttendanceController