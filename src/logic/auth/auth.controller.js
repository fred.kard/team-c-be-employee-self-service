const responseHelper = require('../../helper/response.helper');
const jwt = require('jsonwebtoken');
const imageController = require('../../logic/image/image.controller');
//const { regexp } = require('sequelize/types/lib/operators');
const ic = new imageController();


class AuthController {
    async auth(req, res, next) {
        console.log("masuk")
        const { email, password } = req.body
       
        const user = req.user;
        if (req.businessLogic != undefined) {
            next()
            return
        }
        await user.findAll(
            {
                where:
                {
                    email: email,
                    password: password
                }
            }
        ).then(async data => {
            if (data.length > 0) {
                console.log(data[0].nik)
                const token = jwt.sign({
                    uid: data[0].nik
                }, process.env.SECRET, { expiresIn: '24h' });
                req.user_id = data[0].nik
                req.businessLogic = await responseHelper({
                    "code": 200,
                    "api": `Auth by user : ${email} using password : ${password}`,
                    "message": "Attempt Authentication Success",
                    "entity": "authentication",
                    "state": "attemptAuthenticationSuccess",
                    "data": {
                        "token": token
                    }
                })
                next()
                return
            } else {
                req.user_id = 0
                req.businessLogic = await responseHelper({
                    "code": 401,
                    "message": "Username or password invalid",
                    "entity": "authentication",
                    "state": "attemptAuthenticationError",
                })
                next()
                return
            }
        }
        ).catch(async err => {
            console.log(err)
            req.businessLogic = await responseHelper({
                "code": 500,
                "api": "Auth",
                "entity": "authentication",
                "state": "attemptAuthenticationError",
            })
            next()
            return
        }
        )

    }

    async edit(req, res, next) {
        const user = req.user;
        const { oldpassword,password,confirmpassword } = req.body
        
        
        if (req.businessLogic != undefined) {
            next()
            return
        }
        if (oldpassword == undefined || password == undefined || confirmpassword == undefined
            || oldpassword == "" || password == "" || confirmpassword == "") {
            console.log(req.body)
            req.businessLogic = await responseHelper({
                "code": 401,
                "api": "Edit User",
                "message": "Column Not Complete",
                "entity": "Profile",
                "state": "updateProfileFailed"
            })
            next()
            return
        }
        if (oldpassword == password){
            req.businessLogic = await responseHelper({
                "code": 401,
                "api": "Edit User",
                "message": "Old Password = New Password",
                "entity": "Profile",
                "state": "updateProfileFailed"
            })
            next()
            return 
        }
        if (password != confirmpassword){
            req.businessLogic = await responseHelper({
                "code": 401,
                "api": "Edit User",
                "message": "Password != Confirm Password",
                "entity": "Profile",
                "state": "updateProfileFailed"
            })
            next()
            return 
        }
        await user.findAll({ where: {nik: req.user_id,password:oldpassword } })
            .then(async data => {
                if (data.length > 0) {
                    var date = new Date();
                    date.setDate(date.getDate() + 30);
                    var dateString = date.toISOString().split('T')[0];
                    await user.update({
                        password: password,
                        expireddate: dateString
        
                    }, {
                            where: {
                                nik: req.user_id
                            }
                        }).then(
                            async data => {
                                req.businessLogic = await responseHelper({
                                    "code": 200,
                                    "api": "Update Password",
                                    "message": "Update Password Success",
                                    "entity": "Password",
                                    "state": "updatePasswordSuccess",
                                })
                                next()
                                return
                            }
                        ).catch(
                            async err => {
                                console.log(err)
                                req.businessLogic = await responseHelper({
                                    "code": 500,
                                    "api": "Update Password",
                                    "entity": "Update Password",
                                    "state": "updatePasswordError",
                                })
                                next()
                                return
                            }
                        )
                } else {
                    req.businessLogic = await responseHelper({
                        "code": 401,
                        "api": "Edit Profile",
                        "message": "User Not Found",
                        "entity": "editprofile",
                        "state": "editProfileFailed",
                    })
                    next()
                    return
                }
            })
            .catch(async err => {
                console.log(err)
                req.businessLogic = await responseHelper({
                    "code": 500,
                    "api": "update Profle",
                    "entity": "updateProfile",
                    "state": "upateProfileError",
                })
                next()
                return
            })
    }

    async forget(req, res, next) {
        const user = req.user;
        const { email } = req.body
                
        if (req.businessLogic != undefined) {
            next()
            return
        }
        if (email == undefined 
            || email == "" ) {
            console.log(req.body)
            console.log(req.generatePassword)
            req.businessLogic = await responseHelper({
                "code": 401,
                "api": "Forget Password",
                "message": "Column Not Complete",
                "entity": "Auth",
                "state": "forgetPasswordFailed"
            })
            next()
            return
        }
        await user.findAll({ where: {email: email } })
            .then(async data => {
                if (data.length > 0) {
                    var date = new Date();
                    date.setDate(date.getDate());
                    var dateString = date.toISOString().split('T')[0];
                    await user.update({
                        password: req.generatePassword,
                        expireddate: dateString
                    }, {
                            where: {
                                email: email
                            }
                        }).then(
                            async data => {
                                req.businessLogic = await responseHelper({
                                    "code": 200,
                                    "api": "Reset Password",
                                    "message": "Reset Password Success",
                                    "entity": "Password",
                                    "state": "resetPasswordSuccess",
                                })
                                next()
                                return
                            }
                        ).catch(
                            async err => {
                                console.log(err)
                                req.businessLogic = await responseHelper({
                                    "code": 500,
                                    "api": "Reset Password",
                                    "entity": "Reset Password",
                                    "state": "resetPasswordError",
                                })
                                next()
                                return
                            }
                        )
                } else {
                    req.businessLogic = await responseHelper({
                        "code": 401,
                        "api": "Reset Password",
                        "message": "User Not Found",
                        "entity": "resetPassword",
                        "state": "resetPasswordFailed",
                    })
                    next()
                    return
                }
            })
            .catch(async err => {
                console.log(err)
                req.businessLogic = await responseHelper({
                    "code": 500,
                    "api": "Reset Password",
                    "entity": "resetPassword",
                    "state": "resetPasswordError",
                })
                next()
                return
            })
    }



    async checkexpired(req, res, next) {
        console.log('masuk check expired')
        const user = req.user;
                
        if (req.businessLogic != undefined) {
            next()
            return
        }
        
        await user.findAll({
             where: {nik: req.user_id}
             }
             ).then(async data => {
                if (data.length > 0) {
                    var expireddate = new Date(data[0].expireddate)
                    var date = new Date();
                    date.setDate(date.getDate());
                    var dateString = date.toISOString().split('T')[0];
                    //const diffInMs = Math.abs(expireddate - date)
                    const diffInMs = (expireddate - date)
                    const days =  diffInMs / (1000 * 60 * 60 * 24)
                    if(dateString == data[0].expireddate){
                        req.businessLogic = await responseHelper({
                            "code": 200,
                            "api": "Check Expired",
                            "message": "Password Expired",
                            "entity": "Password",
                            "state": "PasswordExpired",
                            "data" : '0',
                        })
                        next()
                        return
                    }else if (days < 0){
                        req.businessLogic = await responseHelper({
                            "code": 200,
                            "api": "Check Expired",
                            "message": "Password Expired",
                            "entity": "Password",
                            "state": "PasswordExpired",
                            "data" : Math.ceil(days),
                        })
                        next()
                        return
                    }else{
                        req.businessLogic = await responseHelper({
                            "code": 203,
                            "api": "Check Expired",
                            "message": "Password Not Expired",
                            "entity": "Password",
                            "state": "PasswordNotExpired",
                            "data" : Math.ceil(days),
                        })
                        next()
                        return
                    }
                
                } else {
                    req.businessLogic = await responseHelper({
                        "code": 401,
                        "api": "Check Expired Password",
                        "message": "User Not Found",
                        "entity": "checkExpired",
                        "state": "checkExpiredFailed",
                    })
                    next()
                    return
                }
            })
            .catch(async err => {
                console.log(err)
                req.businessLogic = await responseHelper({
                    "code": 500,
                    "api": "Check Expired Password",
                    "entity": "checkexpired",
                    "state": "checkExpiredError",
                })
                next()
                return
            })
    }

}
module.exports = AuthController
