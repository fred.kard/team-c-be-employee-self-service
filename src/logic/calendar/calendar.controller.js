const responseHelper = require('../../helper/response.helper');


class CalendarController {
    async holiday(req, res, next) {
        if (req.businessLogic != undefined) {
            next()
            return
        }
        const public_holiday = req.public_holiday;
        await public_holiday.findAll
        ().then(async data => {
            if (data.length > 0) {
                req.businessLogic = await responseHelper({
                    "code": 200,
                    "message": "Retreive Public Holiday Success",
                    "entity": "calendar",
                    "state": "retreivePublicHolidaySuccess",
                    "data": data
                })
                next()
                return
            } else {
                req.businessLogic = await responseHelper({
                    "code": 200,
                    "message": "Retreive Public Holiday Success",
                    "entity": "calendar",
                    "state": "retreivePublicHolidaySuccess",
                    "data": []
                })
                next()
                return
            }
        }).catch(async err => {
                req.businessLogic = await responseHelper({
                    "code": 500,
                    "entity": "calendar",
                    "state": "retreivePublicHolidayFailed",
                })
                next()
                return
            }
        )
    }

    async insert(req, res, next) {
        var {time_start, time_stop, description} = req.body

        const reminder = req.reminder;

        if (req.businessLogic != undefined) {
            next()
            return
        }

        if (time_start == undefined || time_stop == undefined || description == undefined ||
            time_start == "" || time_stop == "" || description == ""
        ) {
            req.businessLogic = await responseHelper({
                "code": 422,
                "api": "Insert Reminder",
                "message": "Column Not Complete",
                "entity": "Calendar",
                "state": "columnNotComplete"
            })
            next()
            return
        }
        time_start = new Date(time_start)
        time_stop = new Date(time_stop)
        var current = new Date()
        if(time_start.getTime() < current.getTime()){
            req.businessLogic = await responseHelper({
                "code": 422,
                "message": "Cannot be less than current time",
                "entity": "reminder",
                "state": "Cannot be less than current time"
            })
            next()
            return
        }
        if(time_start>time_stop){
            req.businessLogic = await responseHelper({
                "code": 422,
                "message": "Time Stop less then Time Start",
                "entity": "reminder",
                "state": "timeStopLessThenTimeStart"
            })
            next()
            return
        }
        await reminder.create({
            nik: req.user_id,
            time_start: time_start,
            time_stop: time_stop,
            description: description
        }).then(async data => {
            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "Insert Reminder Success",
                "entity": "calendar",
                "state": "insertReminderSuccess",
                "data": data
            })
            next()
            return
        }).catch(async err => {
            console.log(err)
            req.businessLogic = await responseHelper({
                "code": 500,
                "entity": "reminder",
                "state": "insertReminderFailed",
            })
            next()
            return
        })
    }

    async show(req, res, next) {
        const reminder = req.reminder;

        if (req.businessLogic != undefined) {
            next()
            return
        }
       await reminder.findAll({
           where:{
               nik:req.user_id
           },
           order: [["time_start", "ASC"]],
       })
       .then(
        async data=>{
            req.businessLogic = await responseHelper({
                "code": 200,
                "api": "Show Reminder",
                "message": "Get Reminder Success",
                "entity": "Calendar",
                "state": "GetReminderSuccess",
                "data": data
            })
            next()
            return
        }
       )
       .catch(async err =>{
        console.log(err)
        req.businessLogic = await responseHelper({
            "code": 500,
            "entity": "GetCalendar",
            "state": "getCalendarError",
        })
        next()
        return
        } );
    }

    async delete(req, res, next){
        var {id} = req.body;
        const reminder = req.reminder;
        const nik = req.user_id;
        if (req.businessLogic != undefined) {
            next()
            return
        }

        if (id == undefined ||
            id == "" 
        ) {
            req.businessLogic = await responseHelper({
                "code": 422,
                "message": "Column Not Complete",
                "entity": "reminder",
                "state": "columnNotComplete"
            })
            next()
            return
        }
        
        await reminder.destroy({
            where:{
                id:id,
                nik:nik
            }
        }).then(async data => {
            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "Delete Reminder Success",
                "entity": "reminder",
                "state": "deleteReminderSuccess",
                "data": data
            })
            next()
            return
        }).catch(async err => {
            console.log(err)
            req.businessLogic = await responseHelper({
                "code": 500,
                "entity": "reminder",
                "state": "deleteReminderFailed",
            })
            next()
            return
        })

    }

    async edit(req, res, next) {
        var {id, time_start, time_stop, description} = req.body
        const reminder = req.reminder;
        const nik = req.user_id;
        if (req.businessLogic != undefined) {
            next()
            return
        }

        if (id == undefined || time_start == undefined || time_stop == undefined || description == undefined ||
            id == "" || time_start == "" || time_stop == "" || description == ""
        ) {
            req.businessLogic = await responseHelper({
                "code": 422,
                "message": "Column Not Complete",
                "entity": "reminder",
                "state": "columnNotComplete"
            })
            next()
            return
        }
        time_start = new Date(time_start)
        time_stop = new Date(time_stop)
        var current = new Date()
        if(time_start.getTime() < current.getTime()){
            req.businessLogic = await responseHelper({
                "code": 422,
                "message": "Cannot be less than current time",
                "entity": "reminder",
                "state": "Cannot be less than current time"
            })
            next()
            return
        }
        if(time_start>time_stop){
            req.businessLogic = await responseHelper({
                "code": 422,
                "message": "Time Start less then Time Stop",
                "entity": "reminder",
                "state": "timeStopLessThenTimeStop"
            })
            next()
            return
        }
        await reminder.update(
            {
                time_start: time_start,
                time_stop: time_stop,
                description: description,
            },
            {
                where: {
                    id: id,
                    nik: nik
                }
            }).then(async data => {
                req.businessLogic = await responseHelper({
                    "code": 200,
                    "message": "Update Reminders Success",
                    "entity": "reminder",
                    "state": "updateRemindersSuccess",
                    "data": data
                })
                next()
                return
            }).catch(async err => {
                console.log(err)
                req.businessLogic = await responseHelper({
                    "code": 500,
                    "entity": "reminder",
                    "state": "updateReminderFailed",
                })
                next()
                return
            })

    }
}

module.exports = CalendarController
