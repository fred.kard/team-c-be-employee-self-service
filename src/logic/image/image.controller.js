const responseHelper = require('../../helper/response.helper');
const { v4: uuidv4 } = require('uuid');
const fs = require("fs");
const path = require('path')
const slugify = require('slugify');

class ImageController {
    async upload(req, res, next) {
        var imageName
        const profile=req.profile
        if(req.body.avatar != undefined && req.body.avatar != ""){

        let decode = req.body.avatar.split(',')[0];
        let format = decode.split('/')[1].split(';')[0];
        let base64 = req.body.avatar.split(',')[1];
        if(format == "png"){
            format = "png"
        } else if(format == "jpeg"){
            format = "jpg"
        } else{
            req.businessLogic = await responseHelper({
                "code": 401,
                "api": "uploadImage",
                "entity": "image",
                "state": "uploadImageFailed",
            })
            return
        }

        try {
            const file_storage = path.join(__dirname, `../../../data`)
            if (!fs.existsSync(file_storage)) {
                fs.mkdirSync(file_storage);
            }
            const nameSlug = slugify("image");
            const uuid = uuidv4();

            const slug = uuid + '_' + nameSlug;

            const FileName = slug + '.' + format;
            const createdFilePath = file_storage + '/' + slug + '.' + format;

            console.log(req.body.avatar);
            var ReadableData = require('stream').Readable
            const imageBufferData = Buffer.from(base64, "base64")
            var streamObj = new ReadableData()
            streamObj.push(imageBufferData)
            streamObj.push(null)
            streamObj.pipe(fs.createWriteStream(createdFilePath));
            imageName = process.env.IMAGE_URL + FileName;
            req.body.avatar = "encoded into " + FileName;
        }
        catch(error){
            console.log(error)
            req.businessLogic = await responseHelper({
                "code": 500,
                "api": "InternalServerError",
                "entity": "image",
                "state": "uploadImageFailed",
            })
            next()
            return
        }

        await profile.update(
            {
                avatar:imageName
            },
            {
                where:{
                    nik:req.user_id
                }
            }
        ).then(
            async data => {
                req.businessLogic = await responseHelper({
                    "code": 200,
                    "api": "Upload Avatar",
                    "message": "Upload Avatar Success",
                    "entity": "Avatar",
                    "state": "updateAvatarSuccess",
                })
                next()
                return
            }
        ).catch(
            async err => {
                console.log(err)
                req.businessLogic = await responseHelper({
                    "code": 500,
                    "api": "Upload Avatar",
                    "entity": "Update Avatar",
                    "state": "updateAvatarError",
                })
                next()
                return
            }
        )

        } else{
            req.businessLogic = await responseHelper({
                "code": 500,
                "api": "Upload Avatar",
                "entity": "Update Avatar",
                "state": "updateAvatarError",
            })
            next()
            return
        }
        

    }

    async show(req,res,next){
        if (req.businessLogic != undefined) {
            next()
            return
        }
        const file_storage = path.join(__dirname, `../../../data`)
        const file_name = `${file_storage}/${req.params.image_name}`
        console.log(file_name)
            if (!fs.existsSync(file_name)) {
               res.status(500).send({
                    "code": 500,
                    "api": "Show Image",
                    "entity": "image",
                    "state": "showImageFailed",
                    "message": "Image Not Found",
                })
            }
            res.status(200).sendFile(file_name)   
    }

    async get(req,res,next){
        if (req.businessLogic != undefined) {
            next()
            return
        }
        const profile = req.profile
        await profile.findAll({
            where:
            {
                nik:req.user_id
            }
        }).then(
            async data => {
                req.businessLogic = await responseHelper({
                    "code": 200,
                    "api": "Fetch Avatar",
                    "message": "Fetch Avatar Success",
                    "state": "fetchAvatarSuccess",
                    "data":data[0].avatar,
                })
                next()
                return
            }
        ).catch(
            async err => {
                console.log(err)
                req.businessLogic = await responseHelper({
                    "code": 500,
                    "api": "Fetch Avatar",
                    "entity": "Fetch Avatar",
                    "state": "fetchAvatarError",
                })
                next()
                return
            }
        )
    }
}

module.exports = ImageController
