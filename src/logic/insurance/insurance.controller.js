const responseHelper = require('../../helper/response.helper');
const models = require('../../../models');
const medicalModel = models.medical;
const insuranceModel = models.insurance;
const benefitModel = models.benefit;
const userModel = models.profile;

const str_entity = "medical_benefit";

class InsuranceController {
    async insertclaim(req, res, next) {
        var { date, type, usage } = req.body;
        date = new Date(date);

        if (req.businessLogic != undefined) {
            next()
            return
        }

        if (date == undefined || type == undefined || usage == undefined || date == "" || type == "" || usage == "") {
            req.businessLogic = await responseHelper({
                "code": 422,
                "api": "Insert Claim",
                "message": "Please Fill the Empty Data",
                "entity": str_entity,
                "state": "Failed"
            })
            next()
            return
        }

        if (isNaN(usage)) res.status(400).send({ message: 'invalid value, value must be number' })

        let benefitData = await benefitModel.findAndCountAll({ where: { nik: req.user_id } })
            .then(dataInsurance => {
                return dataInsurance;
            })
            .catch(err => {
                res.status(500).send({
                    message:
                        err.message || "Error while retrieving benefit data."
                });
            });
            

        let checkInsurance = await insuranceModel.findOne({ where: { nik: req.user_id, type: type, year: date.getFullYear() } })

        if (usage > checkInsurance.type_balance) {
            req.businessLogic = await responseHelper({
                "code": 422,
                "message": "Sisa saldo plafon anda tidak mecukupi, Silahkan menghubungi HCM ",
                "entity": str_entity,
                "state": "InsertClaimFailed" 
            })
            next()
            return
        } else {
            await medicalModel.create({
                nik: req.user_id,
                date: date,
                year: date.getFullYear(),
                type: type,
                usage: usage
            })
                .then(async data => {

                    let insuranceData = await insuranceModel.findAll({ where: { nik: req.user_id, year: date.getFullYear(), type: type } })
                        .then(dataInsurance => {
                            return dataInsurance;
                        })
                        .catch(err => {
                            res.status(500).send({
                                message:
                                    err.message || "Error while retrieving insurance data."
                            });
                        });

                    var updateBalance = insuranceData[0].type_balance - usage;

                    if (updateBalance < 0 && insuranceData[0].type_usage < benefitData.type_limit) {
                        req.businessLogic = await responseHelper({
                            "code": 422,
                            "message": "Sisa saldo plafon anda tidak mecukupi, Silahkan menghubungi HCM ",
                            "entity": str_entity,
                            "state": "InsertClaimFailed" 
                        })
                        next()
                        return
                    } else {
                        const sum_usage = await medicalModel.sum('usage', {
                            where: { nik: req.user_id, type: type, year: date.getFullYear() },
                        }).then(dataTotal => {
                            return dataTotal
                        }).catch(err => {
                            res.status(500).send({
                                message:
                                    err.message || "Error while retrieving sum data."
                            });
                        })

                        await insuranceModel.update(
                            {
                                type_usage: sum_usage,
                                type_balance: updateBalance
                            },
                            {
                                where: { nik: req.user_id, year: date.getFullYear(), type: type }
                            })

                        let updateInsuranceData = await insuranceModel.findAll({ where: { nik: req.user_id, year: date.getFullYear(), type: type } })
                            .then(dataInsurance => {
                                return dataInsurance;
                            })
                            .catch(err => {
                                res.status(500).send({
                                    message:
                                        err.message || "Error while retrieving data."
                                });
                            });

                        req.businessLogic = await responseHelper({
                            "code": 200,
                            "api": "Insert Claim",
                            "message": "Claim Accepted",
                            "entity": str_entity,
                            "state": "claimSuccess",
                            "data": {
                                "data_usage": data,
                                "sum_usage": sum_usage,
                                "current_balance": updateInsuranceData[0].type_balance
                            }
                        })
                        next()
                        return
                    }
                })
                .catch(async err => {
                    req.businessLogic = await responseHelper({
                        "code": 500,
                        "entity": str_entity,
                        "state": "claim Error"
                    })
                    next()
                    return
                })
            }
    }

    async getAllBenefit(req, res, next) {
        const year = req.body.year;

        if (req.businessLogic != undefined) {
            next()
            return
        }

        await insuranceModel.findAll({
            where: { nik: req.user_id, year: year },
            attributes: ['type', 'type_usage'],
            include: [{ model: models.benefit, attributes: ['type_name', 'type_limit'], where:{nik:req.user_id} }]
        })
            .then(async data => {
                req.businessLogic = await responseHelper({
                    "code": 200,
                    "api": "Show Benefit",
                    "message": "Show Benefit",
                    "entity": str_entity,
                    "state": "showBenefitSuccess",
                    "data": data
                })
                next()
                return
            })
            .catch(async err => {
                console.log(err)
                req.businessLogic = await responseHelper({
                    "code": 500,
                    "entity": str_entity,
                    "state": "showBenefitError"
                })
                next()
                return
            })
    }

    async getAllInfo(req, res, next) {
        const year = req.body.year;

        await userModel.findAll({
            where: { nik: req.user_id }
        })
            .then(async data => {

                const sum_balance = await insuranceModel.sum('type_balance', {
                    where: { nik: req.user_id, year: year },
                }).then(balanceTotal => {
                    return balanceTotal
                }).catch(err => {
                    res.status(500).send({
                        message:
                            err.message || "Error while retrieving sum data."
                    });
                })

                let claimHistory = await medicalModel.findAll({
                    where: { nik: req.user_id, year: year },
                    attributes: ['type', 'date', 'usage'],
                    order: [["date", "DESC"]],
                    include: [{ model: models.benefit, attributes: ['type_name'], where:{nik:req.user_id} }]
                })

                req.businessLogic = await responseHelper({
                    "code": 200,
                    "api": "Show Info",
                    "message": "Show Info",
                    "entity": str_entity,
                    "state": "showInfoSuccess",
                    "data": {
                        "profile": data,
                        "Sisa_Plafon": sum_balance,
                        "History": claimHistory
                    }
                })
                next()
                return

            })
            .catch(async err => {
                req.businessLogic = await responseHelper({
                    "code": 500,
                    "entity": str_entity,
                    "state": "showInfoError"
                })
                next()
                return
            })
    }
}
module.exports = InsuranceController