const responseHelper = require('../../helper/response.helper');
const utility = require("../../helper/utility");
const entity = "leave"

class LeaveController {

    async insertLeave(req, res, next) {
        // 0. INIT
        const res_entity = entity
        let res_code = utility.CODE_SUCCESS
        let res_state = utility.STATE_SUCCESS
        let res_message = ""

        // 1. CUSTOM
        const {type_approval, start_date, end_date, reason} = req.body

        // 2. LOGIC
        if (compareToNow(start_date) <= 0) {
            res_message = "Start date must be equal or larger than today"
        } else if (dateDifference(start_date, end_date) <= 0) {
            res_message = "End date must be equal or larger than start date"
        } else {
            const duration = dateDifferenceExcludeWeekend(start_date, end_date) + 1;

            // 2a. get current user balance
            await req.user_leave.findOne({
                where: {
                    nik: req.user_id,
                    type: type_approval
                }
            }).then(async data => {
                // 2b. check balance found or not
                if (data != null) {
                    const current_balance = data.getDataValue('balance')
                    const new_balance = current_balance - duration

                    if (new_balance >= 0) {
                        // 2c. update balance if new balance >= 0
                        await req.user_leave.update({
                            balance: new_balance
                        }, {
                            where: {
                                nik: req.user_id,
                                type: type_approval
                            }
                        })
                            .then(async () => {
                                // 2d. insert leave
                                await req.leave.create({
                                    nik: req.user_id,
                                    type_approval: type_approval,
                                    start_date: start_date,
                                    end_date: end_date,
                                    reason: reason,
                                    duration: duration
                                }).then(async () => {
                                    res_message = utility.MESSAGE_SUCCESS_INSERT
                                }).catch(async () => {
                                        res_code = utility.CODE_FAILED
                                        res_state = utility.STATE_FAILED
                                        res_message = utility.MESSAGE_FAILED_INSERT
                                    }
                                )
                            })
                            .catch(async () => {
                                res_code = utility.CODE_FAILED
                                res_state = utility.STATE_FAILED
                                res_message = utility.MESSAGE_FAILED_BALANCE
                            })
                    } else {
                        res_message = "Insert failed due to not enough balance"
                    }
                } else {
                    res_message = "Leave balance not found in current user"
                }
            }).catch(async () => {
                res_code = utility.CODE_FAILED
                res_state = utility.STATE_FAILED
                res_message = utility.MESSAGE_FAILED_SELECT
            })

        }

        // 3. RESPONSE
        req.businessLogic = await responseHelper({
            "code": res_code,
            "message": res_message,
            "entity": res_entity,
            "state": res_state
        })
        next()

        function compareToNow(startDate) {
            const now = new Date();
            const date2 = new Date(startDate);
            const diffTime = date2 - now;
            const duration = Math.ceil(diffTime / (1000 * 60 * 60 * 24)) + 1;

            return duration;
        }

        function dateDifference(startDate, endDate) {
            const now = new Date(startDate);
            const date2 = new Date(endDate);
            const diffTime = date2 - now;
            const duration = Math.ceil(diffTime / (1000 * 60 * 60 * 24)) + 1;

            return duration;
        }

        function dateDifferenceExcludeWeekend(start, end) {
            // Copy date objects so don't modify originals
            var s = new Date(start);
            var e = new Date(end);

            // Set time to midday to avoid dalight saving and browser quirks
            s.setHours(12, 0, 0, 0);
            e.setHours(12, 0, 0, 0);

            // Get the difference in whole days
            var totalDays = Math.round((e - s) / 8.64e7);

            // Get the difference in whole weeks
            var wholeWeeks = totalDays / 7 | 0;

            // Estimate business days as number of whole weeks * 5
            var days = wholeWeeks * 5;

            // If not even number of weeks, calc remaining weekend days
            if (totalDays % 7) {
                s.setDate(s.getDate() + wholeWeeks * 7);

                while (s < e) {
                    s.setDate(s.getDate() + 1);

                    // If day isn't a Sunday or Saturday, add to business days
                    if (s.getDay() != 0 && s.getDay() != 6) {
                        ++days;
                    }
                }
            }
            return days;
        }
    }

    async getHistory(req, res, next) {
        // 0. INIT
        // const res_api = `/api/${entity}/getBalance`
        const res_entity = entity
        let res_code = utility.CODE_SUCCESS
        let res_state = utility.STATE_SUCCESS
        let res_message = ""
        let res_data = {}

        // 1. CUSTOM
        // const {nik} = req.body
        // const message = ""

        // 2. LOGIC
        await req.leave.findAll({
                where: {nik: req.user_id},
                order: [
                    ['createdAt', 'DESC']
                ]
            }
        ).then(async data => {
            if (data.length > 0) {
                res_data = data
                res_message = utility.MESSAGE_DATA_FOUND
            } else {
                res_message = utility.MESSAGE_DATA_NOT_FOUND
            }
        }).catch(async () => {
                res_code = utility.CODE_FAILED
                res_state = utility.STATE_FAILED
            }
        )

        // 3. RESPONSE
        req.businessLogic = await responseHelper({
            "code": res_code,
            "message": res_message,
            "entity": res_entity,
            "state": res_state,
            "data": res_data
        })
        next()
    }
}


module.exports = LeaveController
