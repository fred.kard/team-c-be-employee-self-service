const responseHelper = require('../../helper/response.helper');
const models = require('../../../models');

class MeetingController {
    async book(req, res, next) {
        var { room_id, time, description, is_projector, is_hdmi, is_board, is_consumption, consumption } = req.body

        const booking = req.booking;
        const nik = req.user_id;
        if (req.businessLogic != undefined) {
            next()
            return
        }

        if (room_id == undefined ||
            time == undefined ||
            description === undefined ||
            is_projector === undefined ||
            is_hdmi === undefined ||
            is_board === undefined ||
            is_consumption === undefined ||
            consumption === undefined ||
            room_id === "" ||
            time === "" ||
            description === "" ||
            is_projector === "" ||
            is_hdmi === "" ||
            is_board === "" ||
            is_consumption === "" ||
            consumption === ""
        ) {
            req.businessLogic = await responseHelper({
                "code": 422,
                "message": "Column Not Complete",
                "entity": "meeting",
                "state": "columnNotComplete"
            })
            next()
            return
        }
        time = new Date(time)
        var current = new Date()
        if(time.getTime() < current.getTime()){
            req.businessLogic = await responseHelper({
                "code": 422,
                "message": "Cannot be less than current time",
                "entity": "meeting",
                "state": "Cannot be less than current time"
            })
            next()
            return
        }
        await booking.create({
            nik: nik,
            room_id: room_id,
            time: time,
            description: description,
            is_projector: is_projector,
            is_hdmi: is_hdmi,
            is_board: is_board,
            is_consumption: is_consumption,
            consumption: consumption
        }).then(async data => {
            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "Booking Success",
                "entity": "meeting",
                "state": "insertBookingSuccess",
                "data": data
            })
            next()
            return
        }).catch(async err => {
            console.log(err)
            req.businessLogic = await responseHelper({
                "code": 500,
                "entity": "meeting",
                "state": "BookingFailed",
            })
            next()
            return
        })
    }

    async cancel(req, res, next) {
        var { id } = req.body;
        const booking = req.booking;
        const nik = req.user_id;
        if (req.businessLogic != undefined) {
            next()
            return
        }

        if (id == undefined ||
            id == ""
        ) {
            req.businessLogic = await responseHelper({
                "code": 422,
                "message": "Column Not Complete",
                "entity": "meeting",
                "state": "columnNotComplete"
            })
            next()
            return
        }

        await booking.destroy({
            where: {
                id: id,
                nik: nik
            }
        }).then(async data => {
            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "Delete Booking Success",
                "entity": "meeting",
                "state": "deleteBookingSuccess",
                "data": data
            })
            next()
            return
        }).catch(async err => {
            console.log(err)
            req.businessLogic = await responseHelper({
                "code": 500,
                "entity": "meeting",
                "state": "deleteBookingFailed",
            })
            next()
            return
        })

    }

    async mybooking(req, res, next) {
        const profile = models.profile;
        const booking = models.booking;
        const room = models.room;
        await profile.findAll(
            {
                where: { nik: req.user_id },
                include: [
                    {
                        model: booking,
                        as: "booking",
                        include: [
                            room
                        ],
                    },
                ],
                order: [
                    [{ model: booking }, 'time', 'ASC']
                ],
            }
        ).then(
            async data => {
                req.businessLogic = await responseHelper({
                    "code": 200,
                    "message": "My Booking",
                    "entity": "meeting",
                    "state": "fetchMyBookingSuccess",
                    "data": data
                })
                next()
                return
            }
        ).catch(async err => {
            console.log(err)
            req.businessLogic = await responseHelper({
                "code": 500,
                "entity": "meeting",
                "state": "fetchMyBookingFailed",
            })
            next()
            return
        })
    }

    async room(req, res, next) {
        const booking = models.booking;
        const room = models.room;
        await room.findAll(
            {
                include: [
                    {
                        model: booking,
                        as: "booking",
                        attributes: ["time"]
                    }
                ],
                order: [
                    [{ model: booking }, 'time', 'ASC']
                ],
            }
        ).then(
            async data => {
                req.businessLogic = await responseHelper({
                    "code": 200,
                    "message": "Room Schedule",
                    "entity": "meeting",
                    "state": "fetchRoomScheduleSuccess",
                    "data": data
                })
                next()
                return
            }
        ).catch(async err => {
            console.log(err)
            req.businessLogic = await responseHelper({
                "code": 500,
                "entity": "meeting",
                "state": "fetchRoomScheduleFailed",
            })
            next()
            return
        })
    }

}

module.exports = MeetingController
