const responseHelper = require('../../helper/response.helper');

class newsController {
    async show(req, res, next) {
        const news = req.news;

        if (req.businessLogic != undefined) {
            next()
            return
        }
       await news.findAll()
       .then(
        async data=>{
            req.businessLogic = await responseHelper({
                "code": 200,
                "api": "Show News",
                "message": "Get News Success",
                "entity": "News",
                "state": "GetNewsSuccess",
                "data": data
            })
            next()
            return
        }
       )
       .catch(async err =>{
        console.log(err)
        req.businessLogic = await responseHelper({
            "code": 500,
            "entity": "GetNews",
            "state": "getNewsError",
        })
        next()
        return
        } );
    }
}

module.exports = newsController
