const responseHelper = require('../../helper/response.helper');


class notesController {
    async insert(req, res, next) {
        var { date, description } = req.body

        const note = req.note;

        if (req.businessLogic != undefined) {
            next()
            return
        }

        if (date == undefined || description == undefined ||
            date == "" || description == ""
        ) {
            req.businessLogic = await responseHelper({
                "code": 422,
                "api": "Insert Notes",
                "message": "Column Not Complete",
                "entity": "Calendar",
                "state": "columnNotComplete"
            })
            next()
            return
        }
        date = new Date(date)
        await note.create({
            nik: req.user_id,
            date: date,
            description: description
        }).then(async data => {
            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "Insert Notes Success",
                "entity": "calendar",
                "state": "insertNotesSuccess",
                "data": data
            })
            next()
            return
        }).catch(async err => {
            console.log(err)
            req.businessLogic = await responseHelper({
                "code": 500,
                "entity": "calendar",
                "state": "insertNotesFailed",
            })
            next()
            return
        })
    }

    async show(req, res, next) {
        const note = req.note;

        if (req.businessLogic != undefined) {
            next()
            return
        }
        await note.findAll({
            where: {
                nik: req.user_id
            },
            order: [["date", "ASC"]],
        })
            .then(
                async data => {
                    req.businessLogic = await responseHelper({
                        "code": 200,
                        "api": "Show Reminder",
                        "message": "Get Reminder Success",
                        "entity": "Calendar",
                        "state": "GetReminderSuccess",
                        "data": data
                    })
                    next()
                    return
                }
            )
            .catch(async err => {
                console.log(err)
                req.businessLogic = await responseHelper({
                    "code": 500,
                    "entity": "GetNotes",
                    "state": "getNotesError",
                })
                next()
                return
            });
    }

    async delete(req, res, next) {
        var { id } = req.body;
        const note = req.note;
        const nik = req.user_id;
        if (req.businessLogic != undefined) {
            next()
            return
        }

        if (id == undefined ||
            id == ""
        ) {
            req.businessLogic = await responseHelper({
                "code": 422,
                "message": "Column Not Complete",
                "entity": "note",
                "state": "columnNotComplete"
            })
            next()
            return
        }

        await note.destroy({
            where: {
                id: id,
                nik: nik
            }
        }).then(async data => {
            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "Delete Notes Success",
                "entity": "note",
                "state": "deleteNotesSuccess",
                "data": data
            })
            next()
            return
        }).catch(async err => {
            console.log(err)
            req.businessLogic = await responseHelper({
                "code": 500,
                "entity": "note",
                "state": "deleteNoteFailed",
            })
            next()
            return
        })

    }

    async edit(req, res, next) {
        var { id, date, description } = req.body;
        const note = req.note;
        const nik = req.user_id;
        if (req.businessLogic != undefined) {
            next()
            return
        }

        if (id == undefined || date == undefined || description == undefined ||
            id == "" || date == "" || description == ""
        ) {
            req.businessLogic = await responseHelper({
                "code": 422,
                "message": "Column Not Complete",
                "entity": "note",
                "state": "columnNotComplete"
            })
            next()
            return
        }

        await note.update(
            {
                date: date,
                description: description,
            },
            {
                where: {
                    id: id,
                    nik: nik
                }
            }).then(async data => {
                req.businessLogic = await responseHelper({
                    "code": 200,
                    "message": "Update Notes Success",
                    "entity": "note",
                    "state": "updateNotesSuccess",
                    "data": data
                })
                next()
                return
            }).catch(async err => {
                console.log(err)
                req.businessLogic = await responseHelper({
                    "code": 500,
                    "entity": "note",
                    "state": "updateNoteFailed",
                })
                next()
                return
            })

    }
}

module.exports = notesController
