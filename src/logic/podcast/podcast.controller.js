const responseHelper = require('../../helper/response.helper');

class podcastController {
    async show(req, res, next) {
        const podcast = req.podcast;

        if (req.businessLogic != undefined) {
            next()
            return
        }
       await podcast.findAll()
       .then(
        async data=>{
            req.businessLogic = await responseHelper({
                "code": 200,
                "api": "Show Podcast",
                "message": "Get Podcast Success",
                "entity": "Podcast",
                "state": "GetPodcastSuccess",
                "data": data
            })
            next()
            return
        }
       )
       .catch(async err =>{
        console.log(err)
        req.businessLogic = await responseHelper({
            "code": 500,
            "entity": "GetPodcast",
            "state": "getPodcastError",
        })
        next()
        return
        } );
    }
}

module.exports = podcastController
