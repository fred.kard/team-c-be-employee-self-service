const responseHelper = require('../../helper/response.helper');
const profile = require('../../../models').profile;
const relation = require('../../../models').relation;

class ProfileController {
  async getprofile(req, res, next) {
    if (req.businessLogic != undefined) {
      next()
      return
      };
    
    try {
      const dataProfile = await profile.findAll({
        where : { nik: req.user_id },
        include:
           {
            model: relation,
            as: "relation",
            attributes: ["nik", "name", "relation_status", "dob", "address"], 
            }
          });
    
    if (dataProfile.length == 0) {
      req.businessLogic = await responseHelper({
        "code": 422,
        "api": "Get Data Profile",
        "message": "Data not found in database",
        "entity": "Profile",
        "state": "get Profile Failed",
      })
      next()
      return
      };
    
      req.businessLogic = await responseHelper({
        "code": 200,
        "api": "Show User",
        "message": "Get Profile Success",
        "entity": "Profile",
        "state": "getProfileSuccess",
        "data": dataProfile
      })
      next()
      return
          
    } catch(err) {
        console.log("error:", err);
        req.businessLogic = await responseHelper({
          "code": 500,
          "api": "Get Profile",
          "entity": "Profile",
          "state": "get Profile Error",
        })
        next()
        return
      }
    }

  async addRelation (req, res, next) {
    if (req.businessLogic != undefined) {
      next()
      return
    };
        
    try {
      const dataRelation = await relation.findOne({
        where: {
          nik: req.user_id,
          relation_status: req.body.relation_status       
        }
      })
      console.log(dataRelation);
    if (dataRelation != null ) {
      req.businessLogic = await responseHelper({
        "code": 422,
        "api": "Add Data Relation",
        "message": "Data Already Exist",
        "entity": "Relation",
        "state": "Data Already Exist",
      })
      next()
      return 
    } else {
     const data_Relation = await relation.create({
        nik: req.user_id,
        name:req.body.name,
        relation_status: req.body.relation_status,
        dob: req.body.dob,
        address: req.body.address   
      })
      req.businessLogic = await responseHelper({
        "code": 200,
        "api": "Add Data Relation",
        "message": "Add Data Relation Success",
        "entity": "Relation",
        "state": "Add Data Relation Success",
        "data": data_Relation
      })
      next()
      return  
    }
       
  } catch (err) {
      console.log("error:", err);
      req.businessLogic = await responseHelper({
        "code": 500,
        "api": "Add Data Relation",
        "entity": "Relation",
        "state": "Add Data Relation Error, You must insert data in relations_status: ayah, ibu, spouse, anak ke 1, anak ke 2, anak ke 3"
      })
      next()
      return
      }
    }
  async updateProfile (req, res, next) {
    if (req.businessLogic !== undefined) {
      next()
      return
      };
                    
    try {
      const dataProfile = await profile.findOne({
        where: {
          nik: req.user_id
         }
      })
      console.log(dataProfile);
      
    if (dataProfile == undefined || dataProfile == ""){
        req.businessLogic = await responseHelper({
        "code": 422,
        "api": "Update Data Profile",
        "message": "Data Not Found in Database",
        "entity": "Profile",
        "state": "Data Not Found"
      })
      next()
      return 
    } else {
      const data_profile = await profile.update({
        address: req.body.address,
        no_kk: req.body.no_kk
        },{
        where: {
            nik: req.user_id,         
        }  
      })
      req.businessLogic = await responseHelper({
        "code": 200,
        "api": "Update Data Profile",
        "message": "Update Data Profile Success",
        "entity": "Profile",
        "state": "Update Data Profile Success",
        "data": data_profile
      })
      next()
      return
         
    }        
    } catch (err) {
      console.log("error:", err);
      req.businessLogic = await responseHelper({
        "code": 500,
        "api": "Update Data Profile",
        "entity": "Profile",
        "state": "Update Profile Error"
      })
      next()
      return
      }
    }
 
  async updateRelation (req, res, next) {
    if (req.businessLogic != undefined) {
      next()
      return
    };
                    
    try {
      const dataRelation = await relation.findOne({
        where: {
          nik: req.user_id,
          relation_status: req.body.relation_status
         }
      })
    if (dataRelation == null ){
      req.businessLogic = await responseHelper({
        "code": 422,
        "api": "Add Data Relation",
        "message": "Data Not Found in Database",
        "entity": "Relation",
        "state": "Data Not Found",
          })
        next()
        return    
      } else {
        const data_Relation = await relation.update({
          name: req.body.name,
          dob: req.body.dob,
          address: req.body.address 
        }, {
          where: { 
            nik : req.user_id,
            relation_status: req.body.relation_status
            
         } 
      })
        req.businessLogic = await responseHelper({
          "code": 200,
          "api": "Update Data Relation",
          "message": "Update Data Relation Success",
          "entity": "Relation",
          "state": "Update Data Relation Success",
          "data": data_Relation
        })
        next()
        return
      }     
      } catch (err) {
        console.log("error:", err);
        req.businessLogic = await responseHelper({
          "code": 500,
          "api": "Update Data Relation",
          "entity": "Relation",
          "state": "Update Data Relation Error, You must insert data in relations_status: ayah, ibu, spouse, anak ke 1, anak ke 2, anak ke 3"
        })
        next()
        return 
      }
    }
  
}
module.exports = ProfileController
