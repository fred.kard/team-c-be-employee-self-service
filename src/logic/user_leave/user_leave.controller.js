const responseHelper = require('../../helper/response.helper');
const utility = require("../../helper/utility");
const entity = "user_leave"

class User_leaveController {
    async getBalance(req, res, next) {
        // 0. INIT
        // const res_api = `/api/${entity}/getBalance`
        const res_entity = entity
        let res_code = utility.CODE_SUCCESS
        let res_state = utility.STATE_SUCCESS
        let res_message = ""
        let res_data = {}

        // 1. CUSTOM
        // const {nik} = req.body
        // const message = ""

        // 2. LOGIC
        await req.user_leave.findAll({
                where: {nik: req.user_id},
            }
        ).then(async data => {
            if (data.length > 0) {
                res_data = data
                res_message = utility.MESSAGE_DATA_FOUND
            } else {
                res_message = utility.MESSAGE_DATA_NOT_FOUND
            }
        }).catch(async () => {
                res_code = utility.CODE_FAILED
                res_state = utility.STATE_FAILED
            }
        )

        // 3. RESPONSE
        req.businessLogic = await responseHelper({
            "code": res_code,
            "message" : res_message,
            "entity": res_entity,
            "state": res_state,
            "data": res_data
        })
        next()

    }
}

module.exports = User_leaveController
