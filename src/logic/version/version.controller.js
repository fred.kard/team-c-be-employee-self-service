const responseHelper = require('../../helper/response.helper');
const models = require('../../../models');
const strEntity = "Version";

class VersionController {
  async checkVersion(req, res, next) {
    if (req.businessLogic != undefined) {
      next()
      return
    };

    const { current_version, operating_system } = req.body;
    const version = models.version;
    
    if (current_version == undefined || current_version === "" || 
      operating_system == undefined || operating_system === "") 
      {
        req.businessLogic = await responseHelper({
          "code": 422,
          "message": "Version Data not complete",
          "entity": strEntity,
          "state": "CheckVersionFailed"
        })
        next()
        return
      }
    
    if (operating_system != "iOS" && operating_system != "android") {
      req.businessLogic = await responseHelper({
        "code": 422,
        "message": "Operating System Data not valid",
        "entity": strEntity,
        "state": "CheckVersionFailed"
      })
      next()
      return
    }
    
    try {
      const getVersion = await version.findAll({
        where: { operating_system: operating_system }
      })

      if (getVersion.length == 0) {
        req.businessLogic = await responseHelper({
          "code": 200,
          "message": "Version for Operating system not found",
          "entity": strEntity,
          "state": "CheckVersionFailed"
        })
        next()
        return
      }

      if (current_version != getVersion[0].version) {
        var checkResult = "unmatched version! need to update"
      } else {
        var checkResult = "version matched! already latest version" 
      }
      req.businessLogic = await responseHelper({
        "code": 200,
        "message": checkResult,
        "entity": strEntity,
        "state": "CheckVersionSucces",
        "data": getVersion
      })
      next()
      return
      
    } catch(err) {
      req.businessLogic = await responseHelper({
        "code": 500,
        "entity": strEntity,
        "state": "CheckVersionFailed",
      })
      next()
      return
    }
  }

  async latestVersion(req, res, next) {
    if (req.businessLogic != undefined) {
      next()
      return
    };
    
    const { current_version, operating_system, download_url } = req.body;
    const version = models.version;

    if (current_version == undefined || current_version === "" || 
    operating_system == undefined || operating_system === "" ||
    download_url == undefined || download_url === "") 
    {
      req.businessLogic = await responseHelper({
        "code": 422,
        "message": "Latest Version Data not complete",
        "entity": strEntity,
        "state": "UpdateLatestVersionFailed"
      })
      next()
      return
    }
    
    if (operating_system != "iOS" && operating_system != "android") {
      req.businessLogic = await responseHelper({
        "code": 422,
        "message": "Operating System Data not valid",
        "entity": strEntity,
        "state": "UpdateLatestVersionFailed"
      })
      next()
      return
    }

    try {
      const currVersion = await version.findOne({
        where: { operating_system: operating_system }
      })

      if (currVersion == null) {
        const latestVersion = await version.create({
          version: current_version,
          operating_system: operating_system,
          download_url: download_url
        })

        req.businessLogic = await responseHelper({
          "code": 200,
          "message": "Latest Version updated",
          "entity": strEntity,
          "state": "UpdateLatestVersionSuccess",
          "data": latestVersion
        })
        next()
        return
        
      }

      const updateLatestVersion = await version.update(
        {
          version: current_version,
          operating_system: operating_system,
          download_url: download_url
        },
        {
          where: { 
            operating_system: operating_system
          }
        }
      )

      req.businessLogic = await responseHelper({
        "code": 200,
        "message": "Latest Version updated",
        "entity": strEntity,
        "state": "UpdateLatestVersionSuccess",
        "data": updateLatestVersion
      })
      next()
      return

      
    } catch(err) {
      req.businessLogic = await responseHelper({
        "code": 500,
        "entity": strEntity,
        "state": "UpdateLatestVersionFailed",
      })
      next()
      return
    }
  }
}

module.exports = VersionController